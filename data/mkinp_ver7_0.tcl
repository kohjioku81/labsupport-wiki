#!/usr/bin/tclsh

###########################################################################
##########                                                       ##########
##########          SCRIPT FOR MAKING INPUT OF ABINITMP          ########## 
##########                   ( CREST-VERSION )                   ##########
##########                                                       ##########   
##########              ..... version 20060512 .....             ##########
##########                                                       ##########
###########################################################################
#
#  Ver.4.2 => Ver.7.0
#  Move namelist 'MDCNTRL' above namelist 'XYZ'
#

#######################################################
##########    original_to_abinitmp_crest     ##########
#######################################################

proc original_to_abinitmp_crest { } {
    
    global nline_in
    global line_in
    global iline

    global n_cntrl     cntrl
    global n_fmocntrl  fmocntrl
    global n_scf       scf
    global n_basis     basis
    global n_optcntrl  optcntrl
    global n_mlfmo     mlfmo
    global n_mfmo      mfmo
    global n_xuff      xuff
    global n_sczv      sczv
    global n_mp2       mp2
    global n_mp2dns    mp2dns
    global n_mp2grd    mp2grd
    global n_mp3       mp3
    global n_lmp2      lmp2
    global n_dft       dft
    global n_bsse      bsse
    global n_solvation solvation
    global n_pbeq      pbeq
    global n_pop       pop
    global n_gridcntrl gridcntrl

    global n_mcp       mcp
    global n_cis       cis
    global n_cisgrd    cisgrd
    global n_cafi      cafi
    global n_pol       pol
    global n_gf2       gf2
    global n_ccpt      ccpt

    # MD-RELATED ADDED 2013.06.14
    global n_mdcntrl   mdcntrl

    global buffer

    global xyz_line1
    global xyz_line2
    global chk_coord
    global fragment_line1
    global fragment_line2
    global fragpair_line1
    global fragpair_line2

    # MD-RELATED ADDED 2013.06.14
    global vel_line1
    global vel_line2
    global nhc_line1
    global nhc_line2
    global typfrag_line1
    global typfrag_line2

    for {set i 1} {$i<=$nline_in} {incr i} {

	#################
	##### CNTRL #####
	#################
	if {\
		[string first "\&CNTRL" $line_in($i)]>=0 || \
		[string first "\&Cntrl" $line_in($i)]>=0 || \
		[string first "\&cntrl" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_cntrl [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_cntrl} {incr j} {
		set cntrl($j,namelist) $buffer($j,namelist)
		set cntrl($j,value)    $buffer($j,value)
	    }
	}

	####################
	##### FMOCNTRL #####
	####################
	if {\
		[string first "\&FMOCNTRL" $line_in($i)]>=0 || \
		[string first "\&FMOCntrl" $line_in($i)]>=0 || \
		[string first "\&FMOcntrl" $line_in($i)]>=0 || \
		[string first "\&FmoCntrl" $line_in($i)]>=0 || \
		[string first "\&Fmocntrl" $line_in($i)]>=0 || \
		[string first "\&fmocntrl" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_fmocntrl [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_fmocntrl} {incr j} {
		set fmocntrl($j,namelist) $buffer($j,namelist)
		set fmocntrl($j,value)    $buffer($j,value)
	    }
	}

	#################
	##### MLFMO #####
	#################
	if {\
		[string first "\&MLFMO" $line_in($i)]>=0 || \
		[string first "\&MLFmo" $line_in($i)]>=0 || \
		[string first "\&MLfmo" $line_in($i)]>=0 || \
		[string first "\&MlFmo" $line_in($i)]>=0 || \
		[string first "\&Mlfmo" $line_in($i)]>=0 || \
		[string first "\&mlfmo" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mlfmo [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mlfmo} {incr j} {
		set mlfmo($j,namelist) $buffer($j,namelist)
		set mlfmo($j,value)    $buffer($j,value)
	    }
	}

	################
	##### MFMO #####
	################
	if {\
		[string first "\&MFMO" $line_in($i)]>=0 || \
		[string first "\&MFmo" $line_in($i)]>=0 || \
		[string first "\&Mfmo" $line_in($i)]>=0 || \
		[string first "\&MFmo" $line_in($i)]>=0 || \
		[string first "\&Mfmo" $line_in($i)]>=0 || \
		[string first "\&mfmo" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mfmo [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mfmo} {incr j} {
		set mfmo($j,namelist) $buffer($j,namelist)
		set mfmo($j,value)    $buffer($j,value)
	    }
	}

	#################
	##### BASIS #####
	#################
	if {\
		[string first "\&BASIS" $line_in($i)]>=0 || \
		[string first "\&Basis" $line_in($i)]>=0 || \
		[string first "\&basis" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_basis [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_basis} {incr j} {
		set basis($j,namelist) $buffer($j,namelist)
		set basis($j,value)    $buffer($j,value)
	    }
	}

	###############
	##### MCP #####
	###############
	if {\
		[string first "\&MCP" $line_in($i)]>=0 || \
		[string first "\&Mcp" $line_in($i)]>=0 || \
		[string first "\&mcp" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mcp [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mcp} {incr j} {
		set mcp($j,namelist) $buffer($j,namelist)
		set mcp($j,value)    $buffer($j,value)
	    }
	}

	###############
	##### SCF #####
	###############
	if {\
		[string first "\&SCF" $line_in($i)]>=0 || \
		[string first "\&Scf" $line_in($i)]>=0 || \
		[string first "\&scf" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_scf [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_scf} {incr j} {
		set scf($j,namelist) $buffer($j,namelist)
		set scf($j,value)    $buffer($j,value)
	    }
	}

	###############
	##### MP2 #####
	###############
	if {\
		([string first "\&MP2" $line_in($i)]>=0 || \
		 [string first "\&Mp2" $line_in($i)]>=0 || \
		 [string first "\&mp2" $line_in($i)]>=0) && \
		[string first "\&MP2D" $line_in($i)]<0 && \
		[string first "\&Mp2D" $line_in($i)]<0 && \
		[string first "\&mp2D" $line_in($i)]<0 && \
		[string first "\&MP2d" $line_in($i)]<0 && \
		[string first "\&Mp2d" $line_in($i)]<0 && \
		[string first "\&mp2d" $line_in($i)]<0 && \
		[string first "\&MP2G" $line_in($i)]<0 && \
		[string first "\&Mp2G" $line_in($i)]<0 && \
		[string first "\&mp2G" $line_in($i)]<0 && \
		[string first "\&MP2g" $line_in($i)]<0 && \
		[string first "\&Mp2g" $line_in($i)]<0 && \
		[string first "\&mp2g" $line_in($i)]<0 } { \
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mp2 [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mp2} {incr j} {
		set mp2($j,namelist) $buffer($j,namelist)
		set mp2($j,value)    $buffer($j,value)
	    }
	}

	################
	##### XUFF #####
	################
	if {\
		[string first "\&XUFF" $line_in($i)]>=0 || \
		[string first "\&Xuff" $line_in($i)]>=0 || \
		[string first "\&xuff" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_xuff [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_xuff} {incr j} {
		set xuff($j,namelist) $buffer($j,namelist)
		set xuff($j,value)    $buffer($j,value)
	    }
	}

	####################
	##### SCZV #####
	####################
	if {\
		[string first "\&SCZV" $line_in($i)]>=0 || \
		[string first "\&Sczv" $line_in($i)]>=0 || \
		[string first "\&sczv" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_sczv [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_sczv} {incr j} {
		set sczv($j,namelist) $buffer($j,namelist)
		set sczv($j,value)    $buffer($j,value)
	    }
	}


	################
	##### LMP2 #####
	################
	if {\
		[string first "\&LMP2" $line_in($i)]>=0 || \
		[string first "\&Lmp2" $line_in($i)]>=0 || \
		[string first "\&lmp2" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_lmp2 [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_lmp2} {incr j} {
		set lmp2($j,namelist) $buffer($j,namelist)
		set lmp2($j,value)    $buffer($j,value)
	    }
	}

	###############
	##### CIS #####
	###############
	if {\
		([string first "\&CIS" $line_in($i)]>=0 || \
		 [string first "\&Cis" $line_in($i)]>=0 || \
		 [string first "\&cis" $line_in($i)]>=0) && \
		[string first "\&CISG" $line_in($i)]<0 && \
		[string first "\&CisG" $line_in($i)]<0 && \
		[string first "\&cisG" $line_in($i)]<0 && \
		[string first "\&CISg" $line_in($i)]<0 && \
		[string first "\&Cisg" $line_in($i)]<0 && \
		[string first "\&cisg" $line_in($i)]<0 } { 
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_cis [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_cis} {incr j} {
		set cis($j,namelist) $buffer($j,namelist)
		set cis($j,value)    $buffer($j,value)
	    }
	}

        #################
        #### CISGRD #####
        #################
        if {\
                [string first "\&CISGRD" $line_in($i)]>=0 || \
                [string first "\&Cisgrd" $line_in($i)]>=0 || \
                [string first "\&cisgrd" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_cisgrd [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_cisgrd} {incr j} {
                set cisgrd($j,namelist) $buffer($j,namelist)
                set cisgrd($j,value)    $buffer($j,value)
            }
        }

	################
	##### CAFI #####
	################
	if {\
		[string first "\&CAFI" $line_in($i)]>=0 || \
		[string first "\&Cafi" $line_in($i)]>=0 || \
		[string first "\&cafi" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_cafi [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_cafi} {incr j} {
		set cafi($j,namelist) $buffer($j,namelist)
		set cafi($j,value)    $buffer($j,value)
	    }
	}

	###############
	##### POL #####
	###############
	if {\
		[string first "\&POL" $line_in($i)]>=0 || \
		[string first "\&Pol" $line_in($i)]>=0 || \
		[string first "\&pol" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_pol [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_pol} {incr j} {
		set pol($j,namelist) $buffer($j,namelist)
		set pol($j,value)    $buffer($j,value)
	    }
	}

	####################
	##### OPTCNTRL #####
	####################
	if {\
		[string first "\&OPTCNTRL" $line_in($i)]>=0 || \
		[string first "\&OPTCntrl" $line_in($i)]>=0 || \
		[string first "\&OPTcntrl" $line_in($i)]>=0 || \
		[string first "\&OptCntrl" $line_in($i)]>=0 || \
		[string first "\&Optcntrl" $line_in($i)]>=0 || \
		[string first "\&optcntrl" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_optcntrl [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_optcntrl} {incr j} {
		set optcntrl($j,namelist) $buffer($j,namelist)
		set optcntrl($j,value)    $buffer($j,value)
	    }
	}

	#####################
	##### GRIDCNTRL #####
	#####################
	if {\
		[string first "\&GRIDCNTRL" $line_in($i)]>=0 || \
		[string first "\&GridCntrl" $line_in($i)]>=0 || \
		[string first "\&Gridcntrl" $line_in($i)]>=0 || \
		[string first "\&gridcntrl" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_gridcntrl [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_gridcntrl} {incr j} {
		set gridcntrl($j,namelist) $buffer($j,namelist)
		set gridcntrl($j,value)    $buffer($j,value)
	    }
	}

	#################
	##### GF2   #####
	#################
	if {\
		[string first "\&GF2" $line_in($i)]>=0 || \
		[string first "\&Gf2" $line_in($i)]>=0 || \
		[string first "\&gf2" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_gf2 [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_gf2} {incr j} {
		set gf2($j,namelist) $buffer($j,namelist)
		set gf2($j,value)    $buffer($j,value)
	    }
	}

	#################
	##### MP3   #####
	#################
	if {\
		[string first "\&MP3" $line_in($i)]>=0 || \
		[string first "\&Mp3" $line_in($i)]>=0 || \
		[string first "\&mp3" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mp3 [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mp3} {incr j} {
		set mp3($j,namelist) $buffer($j,namelist)
		set mp3($j,value)    $buffer($j,value)
	    }
	}

	#################
	#### MP2DNS #####
	#################
	if {\
		[string first "\&MP2DNS" $line_in($i)]>=0 || \
		[string first "\&Mp2dns" $line_in($i)]>=0 || \
		[string first "\&mp2dns" $line_in($i)]>=0 } {
	    set line1 $i
	    set line2 [get_end $i ]
            set iline $line2
	    set n_mp2dns [get_namelist $line1 $line2]
	    for {set j 1} {$j<=$n_mp2dns} {incr j} {
		set mp2dns($j,namelist) $buffer($j,namelist)
		set mp2dns($j,value)    $buffer($j,value)
	    }
	}

        #################
        #### MP2GRD #####
        #################
        if {\
                [string first "\&MP2GRD" $line_in($i)]>=0 || \
                [string first "\&Mp2grd" $line_in($i)]>=0 || \
                [string first "\&mp2grd" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_mp2grd [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_mp2grd} {incr j} {
                set mp2grd($j,namelist) $buffer($j,namelist)
                set mp2grd($j,value)    $buffer($j,value)
            }
        }

        #################
        ##### CCPT  #####
        #################
        if {\
                [string first "\&CCPT" $line_in($i)]>=0 || \
                [string first "\&ccpt" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_ccpt [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_ccpt} {incr j} {
                set ccpt($j,namelist) $buffer($j,namelist)
                set ccpt($j,value)    $buffer($j,value)
            }
        }

        ################
        ##### DFT  #####
        ################
        if {\
                [string first "\&DFT" $line_in($i)]>=0 || \
                [string first "\&dft" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_dft [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_dft} {incr j} {
                set dft($j,namelist) $buffer($j,namelist)
                set dft($j,value)    $buffer($j,value)
            }
        }

        #################
        ##### BSSE  #####
        #################
        if {\
                [string first "\&BSSE" $line_in($i)]>=0 || \
                [string first "\&bsse" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_bsse [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_bsse} {incr j} {
                set bsse($j,namelist) $buffer($j,namelist)
                set bsse($j,value)    $buffer($j,value)
            }
        }

        #####################
        ##### SOLVATION #####
        #####################
        if {\
                [string first "\&SOLVATION" $line_in($i)]>=0 || \
                [string first "\&Solvation" $line_in($i)]>=0 || \
                [string first "\&solvation" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_solvation [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_solvation} {incr j} {
                set solvation($j,namelist) $buffer($j,namelist)
                set solvation($j,value)    $buffer($j,value)
            }
        }

        ################
        ##### PBE  #####
        ################
        if {\
                [string first "\&PBEQ" $line_in($i)]>=0 || \
                [string first "\&PBEq" $line_in($i)]>=0 || \
                [string first "\&PBeq" $line_in($i)]>=0 || \
                [string first "\&Pbeq" $line_in($i)]>=0 || \
                [string first "\&pbeq" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_pbeq [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_pbeq} {incr j} {
                set pbeq($j,namelist) $buffer($j,namelist)
                set pbeq($j,value)    $buffer($j,value)
            }
        }

        ################
        ##### POP  #####
        ################
        if {\
                [string first "\&POP" $line_in($i)]>=0 || \
                [string first "\&Pop" $line_in($i)]>=0 || \
                [string first "\&pop" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_pop [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_pop} {incr j} {
                set pop($j,namelist) $buffer($j,namelist)
                set pop($j,value)    $buffer($j,value)
            }
        }

        ###################
        ##### MDCNTRL #####
        ###################
        if {\
                [string first "\&MDCNTRL" $line_in($i)]>=0 || \
                [string first "\&MDCntrl" $line_in($i)]>=0 || \
                [string first "\&MDcntrl" $line_in($i)]>=0 || \
                [string first "\&Mdcntrl" $line_in($i)]>=0 || \
                [string first "\&MdCntrl" $line_in($i)]>=0 || \
                [string first "\&mdCntrl" $line_in($i)]>=0 || \
                [string first "\&mdcntrl" $line_in($i)]>=0 } {
            set line1 $i
            set line2 [get_end $i ]
            set iline $line2
            set n_mdcntrl [get_namelist $line1 $line2]
            for {set j 1} {$j<=$n_mdcntrl} {incr j} {
                set mdcntrl($j,namelist) $buffer($j,namelist)
                set mdcntrl($j,value)    $buffer($j,value)
            }
        }

        ###############
        ##### VEL #####
        ###############
        if {\
                [string first "\&VEL" $line_in($i)]>=0 || \
                [string first "\&Vel" $line_in($i)]>=0 || \
                [string first "\&vel" $line_in($i)]>=0 } {
            set vel_line1 [expr $i+1]
            set vel_line2 [expr [get_end $i]-1]
            set iline $vel_line2
        }

        ###############
        ##### NHC #####
        ###############
        if {\
                [string first "\&NHC" $line_in($i)]>=0 || \
                [string first "\&Nhc" $line_in($i)]>=0 || \
                [string first "\&nhc" $line_in($i)]>=0 } {
            set nhc_line1 [expr $i+1]
            set nhc_line2 [expr [get_end $i]-1]
            set iline $nhc_line2
        }

        ####################
        ##### TYPEFRAG #####
        ####################
        if {\
                [string first "\&TYPFRAG" $line_in($i)]>=0 || \
                [string first "\&TYPFrag" $line_in($i)]>=0 || \
                [string first "\&TypFRAG" $line_in($i)]>=0 || \
                [string first "\&TypFrag" $line_in($i)]>=0 || \
                [string first "\&Typfrag" $line_in($i)]>=0 || \
                [string first "\&typfrag" $line_in($i)]>=0 } {
            set typfrag_line1 [expr $i+1]
            set typfrag_line2 [expr [get_end $i]-1]
            set iline $typfrag_line2
        }

	###############
	##### XYZ #####
	###############
	if {\
		[string first "\&XYZ" $line_in($i)]>=0 || \
		[string first "\&Xyz" $line_in($i)]>=0 || \
		[string first "\&xyz" $line_in($i)]>=0 } {
	    if { [llength $line_in($i)]>1 } {

		### GAMESS OUTPUT TYPE COORDINATE
		if {\
			[string first "GAMESS-OUT" $line_in($i)]>=0 || \
			[string first "GAMESS-out" $line_in($i)]>=0 || \
			[string first "gamess-out" $line_in($i)]>=0 || \
			[string first "Gamess-Out" $line_in($i)]>=0 || \
			[string first "Gamess-out" $line_in($i)]>=0 } {
		    set chk_coord 1
		}

		### GAUSSIAN OUTPUT TYPE COORDINATE
		if {\
			[string first "GAUSSIAN-OUT" $line_in($i)]>=0 || \
			[string first "GAUSSIAN-out" $line_in($i)]>=0 || \
			[string first "gaussian-out" $line_in($i)]>=0 || \
			[string first "Gaussian-Out" $line_in($i)]>=0 || \
			[string first "Gaussian-out" $line_in($i)]>=0 } {
		    set chk_coord 2
		}
	    }
		
	    set xyz_line1 [expr $i+1]
	    set xyz_line2 [expr [get_end $i]-1]
            set iline $xyz_line2
	}

	####################
	##### FRAGMENT #####
	####################
	if {\
		[string first "\&FRAGMENT" $line_in($i)]>=0 || \
		[string first "\&Fragment" $line_in($i)]>=0 || \
		[string first "\&fragment" $line_in($i)]>=0 } {
	    set fragment_line1 [expr $i+1]
	    set fragment_line2 [expr [get_end $i]-1]
            set iline $fragment_line2
	}

        ####################
        ##### FRAGPAIR #####
        ####################
        if {\
                [string first "\&FRAGPAIR" $line_in($i)]>=0 || \
                [string first "\&FragPair" $line_in($i)]>=0 || \
                [string first "\&Fragpair" $line_in($i)]>=0 || \
                [string first "\&fragpair" $line_in($i)]>=0 } {
            set fragpair_line1 [expr $i+1]
            set fragpair_line2 [expr [get_end $i]-1]
            set iline $fragpair_line2
        }

    }
    
}

####################################
##########    get_end     ##########
####################################

proc get_end {iline} {

    global nline_in line_in

    for {set i $iline} {$i<=$nline_in} {incr i} {
	if {\
		[string first "\&END" $line_in($i)]>=0 || \
		[string first "\&End" $line_in($i)]>=0 || \
		[string first "\&end" $line_in($i)]>=0 } {
	    return $i
	}
	if { [string first "\/" $line_in($i)]>=0 } {
	    if { [string index [string trimright $line_in($i)] end]=="\/" } {
		return $i
	    }
	}
    }
}

########################################
##########    get_namelist    ##########
########################################

proc get_namelist {line1 line2} {

    global line_in buffer
    
    set n_name_list 0

    for {set i $line1} {$i<=$line2} {incr i} {

	set leng [string length $line_in($i)]

	for {set j 0} {$j<=$leng} {incr j} {
	    if {[string index $line_in($i) $j]=="\="} {
		
		### get namelist ###
		set tmp1 [string trimright [string range $line_in($i) 0 [expr $j-1]]]
		set tmp2 [lindex $tmp1 [expr [llength $tmp1]-1]]
		set namelist $tmp2

		### get value ###
		set tmp1 [string trimleft [string range $line_in($i) [expr $j+1] $leng]]
		set tmp2 [lindex $tmp1 0]
		set value $tmp2
		set first_char [string index $value 0]
		set last_char  [string index $value [expr [string length $value]-1]]
		if {$first_char=="\'" && $last_char!="\'"} {
		    set tmp1_leng [string length $tmp1]
		    for {set k 2} {$k<=$tmp1_leng} {incr k} {
			if {[string index $tmp1 $k]=="\'"} {
			    set pos $k
			    break
			}
		    }
		    set value [string rang $tmp1 0 $pos]
		}
		if {$first_char=="\"" && $last_char!="\""} {
		    set tmp1_leng [string length $tmp1]
		    for {set k 2} {$k<=$tmp1_leng} {incr k} {
			if {[string index $tmp1 $k]=="\""} {
			    set pos $k
			    break
			}
		    }
		    set value [string rang $tmp1 0 $pos]
		}
		### set namelist and value ###
                if {[string first "=" $namelist]<0} {
		  set n_name_list [expr $n_name_list+1]
		  set buffer($n_name_list,namelist) $namelist
		  set buffer($n_name_list,value)    $value
                }
	    }
	}
    }
    return $n_name_list
} 

proc gamess_out_to_abinitmp {gamess_in} {

    global atom atom_count
    
    set atomic_number [expr int([expr [lindex $gamess_in 1]+0.1])]
    set coord_x       [lindex $gamess_in 2]
    set coord_y       [lindex $gamess_in 3]
    set coord_z       [lindex $gamess_in 4]
    set atomic_symbol $atom($atomic_number,symbol)

    set atom_count [ expr $atom_count + 1 ]

    set abinit_out [format "%6s %6s %6s %14.8f %14.8f %14.8f %2s" \
			$atom_count $atomic_symbol "1" \
			$coord_x $coord_y $coord_z "1"]

    return $abinit_out
}


proc gaussian_out_to_abinitmp {gaussian_in} {

    global atom atom_count

    set atomic_number [lindex $gaussian_in 1]
    set coord_x       [lindex $gaussian_in 3]
    set coord_y       [lindex $gaussian_in 4]
    set coord_z       [lindex $gaussian_in 5]
    set atomic_symbol $atom($atomic_number,symbol)

    set atom_count [ expr $atom_count + 1 ]

    set abinit_out [format "%6s %6s %6s %14.8f %14.8f %14.8f %2s" \
			$atom_count $atomic_symbol "1" \
			$coord_x $coord_y $coord_z "1"]

    return $abinit_out
}

proc mk_atomic_table {} {

    global atom

    ## initialize !!
    for {set i 1} {$i<=111} {incr i} {
	set atom($i,symbol) "-"
    }

    set atom(1,symbol)   "H"
    set atom(2,symbol)   "He"
    set atom(3,symbol)   "Li"
    set atom(4,symbol)   "Be"
    set atom(5,symbol)   "B"
    set atom(6,symbol)   "C"
    set atom(7,symbol)   "N"
    set atom(8,symbol)   "O"
    set atom(9,symbol)   "F"
    set atom(10,symbol)  "Ne"
    set atom(11,symbol)  "Na"
    set atom(12,symbol)  "Mg"
    set atom(13,symbol)  "Al"
    set atom(14,symbol)  "Si"
    set atom(15,symbol)  "P"
    set atom(16,symbol)  "S"
    set atom(17,symbol)  "Cl"
    set atom(18,symbol)  "Ar"
    set atom(19,symbol)  "K"
    set atom(20,symbol)  "Ca"
    set atom(21,symbol)  "Sc"
    set atom(22,symbol)  "Ti"
    set atom(23,symbol)  "V"
    set atom(24,symbol)  "Cr"
    set atom(25,symbol)  "Mn"
    set atom(26,symbol)  "Fe"
    set atom(27,symbol)  "Co"
    set atom(28,symbol)  "Ni"
    set atom(29,symbol)  "Cu"
    set atom(30,symbol)  "Zn"
    set atom(31,symbol)  "Ga"
    set atom(32,symbol)  "Ge"
    set atom(33,symbol)  "As"
    set atom(34,symbol)  "Se"
    set atom(35,symbol)  "Br"
    set atom(36,symbol)  "Kr"
    set atom(37,symbol)  "Rb"
    set atom(38,symbol)  "Sr"
    set atom(39,symbol)  "Y"
    set atom(40,symbol)  "Zr"
    set atom(41,symbol)  "Nb"
    set atom(42,symbol)  "Mo"
    set atom(43,symbol)  "Tc"
    set atom(44,symbol)  "Ru"
    set atom(45,symbol)  "Rh"
    set atom(46,symbol)  "Pd"
    set atom(47,symbol)  "Ag"
    set atom(48,symbol)  "Cd"
    set atom(49,symbol)  "In"
    set atom(50,symbol)  "Sn"
    set atom(51,symbol)  "Sb"
    set atom(52,symbol)  "Te"
    set atom(53,symbol)  "I"
    set atom(54,symbol)  "Xe"
    set atom(55,symbol)  "Cs"
    set atom(56,symbol)  "Ba"
    set atom(57,symbol)  "La"
    set atom(58,symbol)  "Ce"
    set atom(59,symbol)  "Pr"
    set atom(60,symbol)  "Nd"
    set atom(61,symbol)  "Pm"
    set atom(62,symbol)  "Sm"
    set atom(63,symbol)  "Eu"
    set atom(64,symbol)  "Gd"
    set atom(65,symbol)  "Tb"
    set atom(66,symbol)  "Dy"
    set atom(67,symbol)  "Ho"
    set atom(68,symbol)  "Er"
    set atom(69,symbol)  "Tm"
    set atom(70,symbol)  "Yb"
    set atom(71,symbol)  "Lu"
    set atom(72,symbol)  "Hf"
    set atom(73,symbol)  "Ta"
    set atom(74,symbol)  "W"
    set atom(75,symbol)  "Re"
    set atom(76,symbol)  "Os"
    set atom(77,symbol)  "Ir"
    set atom(78,symbol)  "Pt"
    set atom(79,symbol)  "Au"
    set atom(80,symbol)  "Hg"
    set atom(81,symbol)  "Tl"
    set atom(82,symbol)  "Pb"
    set atom(83,symbol)  "Bi"
    set atom(84,symbol)  "Po"
    set atom(85,symbol)  "At"
    set atom(86,symbol)  "Rn"
    set atom(87,symbol)  "Fr"
    set atom(88,symbol)  "Ra"
    set atom(89,symbol)  "Ac"
    set atom(90,symbol)  "Th"
    set atom(91,symbol)  "Pa"
    set atom(92,symbol)  "U"
    set atom(93,symbol)  "Np"
    set atom(94,symbol)  "Pu"
    set atom(95,symbol)  "Am"
    set atom(96,symbol)  "Cm"
    set atom(97,symbol)  "Bk"
    set atom(98,symbol)  "Cf"
    set atom(99,symbol)  "Es"
    set atom(100,symbol) "Fm"
    set atom(101,symbol) "Md"
    set atom(102,symbol) "No"
    set atom(103,symbol) "Lr"
    set atom(104,symbol) "Rf"
    set atom(105,symbol) "Db"
    set atom(106,symbol) "Sg"
    set atom(107,symbol) "Bh"
    set atom(108,symbol) "Hs"
    set atom(109,symbol) "Mt"
    set atom(110,symbol) "Ds"
    set atom(111,symbol) "Rg"

}

#################################
##########    MAIN     ##########
#################################

###
### initialize
###

set nline_in    0
set iline       0

set n_cntrl     0
set n_fmocntrl  0
set n_mlfmo     0
set n_mfmo      0
set n_basis     0
set n_mcp       0
set n_scf       0
set n_xuff      0
set n_sczv      0
set n_mp2       0
set n_lmp2      0
set n_cis       0
set n_cisgrd    0
set n_cafi      0
set n_pol       0
set n_optcntrl  0
set n_gridcntrl 0
set n_dft       0
set n_bsse      0
set n_solvation 0
set n_pbeq      0
set n_pop       0

set n_gf2       0
set n_mp3       0
set n_mp2dns    0
set n_mp2grd    0
set n_ccpt      0

set n_mdcntrl   0

set xyz_line1 1
set xyz_line2 0
set fragment_line1 1
set fragment_line2 0
set fragpair_line1 1
set fragpair_line2 0

set vel_line1 1
set vel_line2 0
set nhc_line1 1
set nhc_line2 0
set typfrag_line1 1
set typfrag_line2 0

### chk_coord --> 0:ABINIT-MP , 1:GAMESS-OUT , 2:GAUSSIAN-OUT 
set chk_coord  0  
set atom_count 0

### set atomic table
mk_atomic_table

###
### read original input
###

while {1} {
    gets stdin temp
    if {[llength $temp]>0} {
	set HeadChar [string index [string trim $temp] 0]
	if { $HeadChar!="\*" ||
	     $HeadChar!="\!" ||
	     $HeadChar!="\#" } {
	    set nline_in [expr $nline_in + 1]
	    set line_in($nline_in) $temp
	}
    }
    if {[eof stdin]==1} { break }
}

###
### original input to abinitmp ( crest-version ) input
###

original_to_abinitmp_crest

###
### out abinitmp ( crest-version ) ajf-file
###

## CNTRL
puts "\&CNTRL"
for {set i 1} {$i<=$n_cntrl} {incr i} {
    puts "  $cntrl($i,namelist)\=$cntrl($i,value)"
}
#puts "\&END"
puts "\/"

## FMOCNTRL
puts "\&FMOCNTRL"
for {set i 1} {$i<=$n_fmocntrl} {incr i} {
    puts "  $fmocntrl($i,namelist)\=$fmocntrl($i,value)"
}
#puts "\&END"
puts "\/"

## SCF
puts "\&SCF"
for {set i 1} {$i<=$n_scf} {incr i} {
    puts "  $scf($i,namelist)\=$scf($i,value)"
}
#puts "\&END"
puts "\/"

## BASIS
puts "\&BASIS"
for {set i 1} {$i<=$n_basis} {incr i} {
    puts "  $basis($i,namelist)\=$basis($i,value)"
}
#puts "\&END"
puts "\/"

## OPTCNTRL
puts "\&OPTCNTRL"
for {set i 1} {$i<=$n_optcntrl} {incr i} {
    puts "  $optcntrl($i,namelist)\=$optcntrl($i,value)"
}
#puts "\&END"
puts "\/"

## MLFMO (-> MFMO in the future)
puts "\&MLFMO"
for {set i 1} {$i<=$n_mlfmo} {incr i} {
    puts "  $mlfmo($i,namelist)\=$mlfmo($i,value)"
}
#puts "\&END"
puts "\/"

## MFMO
puts "\&MFMO"
for {set i 1} {$i<=$n_mfmo} {incr i} {
    puts "  $mfmo($i,namelist)\=$mfmo($i,value)"
}
#puts "\&END"
puts "\/"

## XUFF
puts "\&XUFF"
for {set i 1} {$i<=$n_xuff} {incr i} {
    puts "  $xuff($i,namelist)\=$xuff($i,value)"
}
#puts "\&END"
puts "\/"

## SCZV
puts "\&SCZV"
for {set i 1} {$i<=$n_sczv} {incr i} {
    puts "  $sczv($i,namelist)\=$sczv($i,value)"
}
#puts "\&END"
puts "\/"
    
## MP2
puts "\&MP2"
for {set i 1} {$i<=$n_mp2} {incr i} {
    puts "  $mp2($i,namelist)\=$mp2($i,value)"
}
#puts "\&END"
puts "\/"

## MP2DNS
puts "\&MP2DNS"
for {set i 1} {$i<=$n_mp2dns} {incr i} {
    puts "  $mp2dns($i,namelist)\=$mp2dns($i,value)"
}
#puts "\&END"
puts "\/"

## MP2GRD
puts "\&MP2GRD"
for {set i 1} {$i<=$n_mp2grd} {incr i} {
    puts "  $mp2grd($i,namelist)\=$mp2grd($i,value)"
}
#puts "\&END"
puts "\/"

## MP3
puts "\&MP3"
for {set i 1} {$i<=$n_mp3} {incr i} {
    puts "  $mp3($i,namelist)\=$mp3($i,value)"
}
#puts "\&END"
puts "\/"

## LMP2
puts "\&LMP2"
for {set i 1} {$i<=$n_lmp2} {incr i} {
    puts "  $lmp2($i,namelist)\=$lmp2($i,value)"
}
#puts "\&END"
puts "\/"

## DFT
puts "\&DFT"
for {set i 1} {$i<=$n_dft} {incr i} {
    puts "  $dft($i,namelist)\=$dft($i,value)"
}
#puts "\&END"
puts "\/"

## BSSE
puts "\&BSSE"
for {set i 1} {$i<=$n_bsse} {incr i} {
    puts "  $bsse($i,namelist)\=$bsse($i,value)"
}
#puts "\&END"
puts "\/"

## FRAGPAIR
puts "\&FRAGPAIR"
for {set i $fragpair_line1} {$i<=$fragpair_line2} {incr i} {
    puts $line_in($i)
}

##
for {set i [expr $iline+1]} {$i<=$nline_in} {incr i} {
    set tmp1 [lindex $line_in($i) 0]
    if {\
	    [string first "\&END" $tmp1]>=0 || \
	    [string first "\&End" $tmp1]>=0 || \
	    [string first "\&end" $tmp1]>=0 || \
	    [string first "\/"    $tmp1]>=0 } {
      # DO NOTHING
    } else {
      puts $line_in($i)
    }
}
puts "\/"

## SOLVATION
puts "\&SOLVATION"
for {set i 1} {$i<=$n_solvation} {incr i} {
    puts "  $solvation($i,namelist)\=$solvation($i,value)"
}
#puts "\&END"
puts "\/"

## PBEQ
puts "\&PBEQ" 
for {set i 1} {$i<=$n_pbeq} {incr i} {
    puts "  $pbeq($i,namelist)\=$pbeq($i,value)"
}
#puts "\&END"
puts "\/"

## POP
puts "\&POP"
for {set i 1} {$i<=$n_pop} {incr i} {
    puts "  $pop($i,namelist)\=$pop($i,value)"
}
#puts "\&END"
puts "\/"

## GRIDCNTRL
puts "\&GRIDCNTRL"
for {set i 1} {$i<=$n_gridcntrl} {incr i} {
    puts "  $gridcntrl($i,namelist)\=$gridcntrl($i,value)"
}
#puts "\&END"
puts "\/"

## MCP
puts "\&MCP"
for {set i 1} {$i<=$n_mcp} {incr i} {
    puts "  $mcp($i,namelist)\=$mcp($i,value)"
}
#puts "\&END"
puts "\/"

## CIS
puts "\&CIS"
for {set i 1} {$i<=$n_cis} {incr i} {
    puts "  $cis($i,namelist)\=$cis($i,value)"
}
#puts "\&END"
puts "\/"

## CISGRD
puts "\&CISGRD"
for {set i 1} {$i<=$n_cisgrd} {incr i} {
    puts "  $cisgrd($i,namelist)\=$cisgrd($i,value)"
}
#puts "\&END"
puts "\/"

## CAFI
puts "\&CAFI"
for {set i 1} {$i<=$n_cafi} {incr i} {
    puts "  $cafi($i,namelist)\=$cafi($i,value)"
}
#puts "\&END"
puts "\/"

## POL
puts "\&POL"
for {set i 1} {$i<=$n_pol} {incr i} {
    puts "  $pol($i,namelist)\=$pol($i,value)"
}
#puts "\&END"
puts "\/"

## GF2
puts "\&GF2"
for {set i 1} {$i<=$n_gf2} {incr i} {
    puts "  $gf2($i,namelist)\=$gf2($i,value)"
}
#puts "\&END"
puts "\/"

## CCPT
puts "\&CCPT"
for {set i 1} {$i<=$n_ccpt} {incr i} {
    puts "  $ccpt($i,namelist)\=$ccpt($i,value)"
}
#puts "\&END"
puts "\/"

## XYZ
puts "\&XYZ"
for {set i $xyz_line1} {$i<=$xyz_line2} {incr i} {
    
    switch $chk_coord {
	1 { puts [ gamess_out_to_abinitmp $line_in($i) ] }
	2 { puts [ gaussian_out_to_abinitmp $line_in($i) ] }
	default { puts $line_in($i) }
    }
    
}
puts "\/"

## FRAGMENT
puts "\&FRAGMENT"
for {set i $fragment_line1} {$i<=$fragment_line2} {incr i} {
    puts $line_in($i)
}
puts "\/"

## MDCNTRL
puts "\&MDCNTRL"
for {set i 1} {$i<=$n_mdcntrl} {incr i} {
    puts "  $mdcntrl($i,namelist)\=$mdcntrl($i,value)"
}
#puts "\&END"
puts "\/"

## VEL
puts "\&VEL"
for {set i $vel_line1} {$i<=$vel_line2} {incr i} {
    puts $line_in($i)
}
puts "\/"

## NHC
puts "\&NHC"
for {set i $nhc_line1} {$i<=$nhc_line2} {incr i} {
    puts $line_in($i)
}
puts "\/"

## TYPFRAG
puts "\&TYPEFRAG"
for {set i $typfrag_line1} {$i<=$typfrag_line2} {incr i} {
    puts $line_in($i)
}
puts "\/"
