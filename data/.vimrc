" ~/.pyenv/shimsを$PATHに追加
let $PATH = "~/.pyenv/shims:".$PATH
set nocompatible
" filetype plugin indent off
let OSTYPE = system('uname')

" ------------------
" Neobundle setting
" ------------------

if has('vim_starting')
    set runtimepath+=~/.vim/bundle/neobundle.vim
    call neobundle#begin(expand('~/.vim/bundle'))

    " neobundle
    NeoBundleFetch 'Shougo/neobundle.vim'

    " auto complete
    " if OSTYPE == 'Linux\n'
    " NeoBundle 'davidhalter/jedi-vim'
    " endif
    " NeoBundle 'Shougo/neocomplete.vim'

    " file manager
    NeoBundle 'Shougo/unite.vim'
    NeoBundle 'Shougo/neomru.vim'
    NeoBundle 'Shougo/neoyank.vim'
    NeoBundle 'Shougo/vimproc.vim', {
        \ 'build' : {
            \     'windows' : 'tools\\update-dll-mingw',
            \     'cygwin' : 'make -f make_cygwin.mak',
            \     'mac' : 'make',
            \     'linux' : 'make',
            \     'unix' : 'gmake',
            \    },
            \ }
    NeoBundle 'Shougo/unite-outline'
    NeoBundle 'Shougo/vimfiler.git'
    " NeoBundle 'scrooloose/nerdtree'

    " tag jump"
    " NeoBundle 'szw/vim-tags'

    " testrun from vim
    NeoBundle 'thinca/vim-quickrun'

    " check syntax
    NeoBundle 'scrooloose/syntastic'

    " visualize indent
    NeoBundle 'nathanaelkane/vim-indent-guides'
    "
    " check whitespace
    NeoBundle 'bronson/vim-trailing-whitespace'

    " color
    NeoBundle 'tomasr/molokai'

    " move easily
    NeoBundle 'easymotion/vim-easymotion'

    NeoBundle 'itchyny/lightline.vim'

    " vim git
    NeoBundle 'tpope/vim-fugitive'
    " pyenv 処理用に vim-pyenv を追加
    " Note: depends が指定されているため jedi-vim より後にロードされる（ことを期待）
    " if OSTYPE == "Linux\n"
    "     NeoBundleLazy "lambdalisue/vim-pyenv", {
    "          \ "depends": ['davidhalter/jedi-vim'],
    "          \ "autoload": {
    "          \   "filetypes": ["python", "python3", "djangohtml"]
    "          \ }}
    " endif

    NeoBundle 'tweekmonster/braceless.vim'

    " others
    " NeoBundle 'tomtom/tcomment_vim'
    " NeoBundle 'grep.vim'
    " NeoBundle 'itchyny/lightline.vim'
    " NeoBundle 'Shougo/neosnippet-snippets'
    " NeoBundle 'Townk/vim-autoclose'
    " NeoBundle 'mattn/emmet-vim'
    " NeoBundle 'Shougo/neosnippet.vim'
    " NeoBundle 'mattn/emmet-vim'
    " NeoBundle 'kana/vim-smartinput'
    " NeoBundle 'kana/vim-operator-user'
    " NeoBundle 'kana/vim-textobj-user'
    " NeoBundle 'kana/vim-operator-replace'
    " NeoBundle 'rhysd/vim-operator-surround'
    " NeoBundle 'andviro/flake8-vim'
    " NeoBundle 'hynek/vim-python-pep8-indent'
    " NeoBundle 'jmcantrell/vim-virtualenv'
    " NeoBundle 'hachibeeDI/python_hl_lvar.vim'
    " NeoBundle 'kana/vim-textobj-indent'
    " NeoBundle 'bps/vim-textobj-python'
    " NeoBundle 'hachibeeDI/smartinput-petterns'

    call neobundle#end()
endif


" ------------
" for general
" ------------

    " filetype plugin indent on
    " set term=builtin_linux
    " set ttytype=builtin_linux

    syntax on
    " line number
    set number
    set laststatus=2

    " tab info
    set tabstop=4
    set shiftwidth=4
    set softtabstop=4

    set showmatch
    set showcmd
    set backspace=indent,eol,start

    " GUI
    set guioptions+=a

    " CUI
    set clipboard+=unnamed,autoselect
    set autoindent
    set smartindent

    set hlsearch
    set smartcase
    set list
    set listchars=tab:>-,trail:.
    set cursorline

    " auto comment off
    augroup auto_comment_off
        autocmd!
        autocmd BufEnter * setlocal formatoptions-=r
        autocmd BufEnter * setlocal formatoptions-=o
    augroup END

    set hidden
    set wildmenu
    " set foldmethod=indent

    set expandtab
    " enable vim alias
    let $BASH_ENV = "~/.bash_alias"

    " color
    autocmd ColorScheme * highlight Search ctermfg=226 ctermbg=18 cterm=bold,reverse
    colorscheme molokai
    set t_Co=256
    set clipboard=unnamed
    let fortran_free_source=1
    let fortran_fold=1
    au! BufRead,BufNewFile *.f90 let b:fortran_do_enddo=1

    autocmd QuickFixCmdPost *grep* cwindow

    " ime setting
    " Insert mode: lmap off, IME ON
    set iminsert=2
    " Serch mode: lmap off, IME ON
    set imsearch=2
    " Normal mode: IME off
    inoremap <silent> <ESC> <ESC>:set iminsert=0<CR>

" ----------------
" keymap original
" ----------------

    let mapleader = "\<Space>"

    noremap j gj
    noremap k gk
    " noremap q b
    vmap z ge
    noremap q; q:

    noremap <leader>h ^
    noremap <leader>l $
    nnoremap <Leader>w :w<CR>
    nnoremap <Leader>q :wq
    nnoremap <Leader><Leader>q :q!
    noremap <Leader>p :a!<CR>
    cnoremap <C-p> <C-r>0
    nnoremap <Leader>a Ea
    nnoremap <Leader>4 $
    nmap <Leader>9 A)<Esc>
    " noremap <Leader>/ q:

    noremap <Leader>j }
    noremap <Leader>k {

    nnoremap <Leader>d <C-d>
    nnoremap <Leader>b :b
    nnoremap U <C-u>
    nnoremap Y <C-b>
    nnoremap F <C-f>
    nnoremap D <C-d>
    nnoremap J <C-d>
    nnoremap K <C-u>
    " nnoremap <leader>g <S-g>

    nnoremap <Leader>v <C-v>
    vnoremap <Leader><tab> I<tab><esc>:FixWhitespace<CR>
    nnoremap <leader><leader>y vEy

    nnoremap <leader>" i"<esc>Ea"<esc>
    nnoremap <leader>' i'<esc>Ea'<esc>

    " A insert
    vmap <Leader>a $A

    " move last
    vmap <Leader>4 $

    " comment out
    map <Leader>3 I# <Esc>
    map <Leader>2 I" <Esc>
    map <Leader>1 I! <Esc>
    map <Leader>c I# <Esc>
    map <Leader>x I" <Esc>
    map <Leader>z I! <Esc>

    map <Leader><Leader>c :s/# //g<CR>:nohlsearch<CR>
    map <Leader><Leader>x :s/" //g<CR>:nohlsearch<CR>
    map <Leader><Leader>z :s/! //g<CR>:nohlsearch<CR>
    map <Leader><Leader>3 :s/# //g<CR>:nohlsearch<CR>
    map <Leader><Leader>2 :s/" //g<CR>:nohlsearch<CR>
    map <Leader><Leader>1 :s/! //g<CR>:nohlsearch<CR>

    " tab jump"
    nnoremap <Leader>] g<C-]>
    nnoremap <Leader>t <C-t>
    nnoremap <C-]> g<C-]>

    " escape by using jj key"
    inoremap <silent>jj <Esc>
    vnoremap <silent>m <Esc>
    " vnoremap <silent>jj <Esc>

    " cancel highlight esc"
    nmap <Esc><Esc> :nohlsearch<CR><Esc>

    " tab automatically"
    nmap <Leader><tab> :set tabstop=4<CR>:set shiftwidth=4<CR>:set softtabstop=4<CR>:set expandtab<CR>:retab<CR>:
    nmap <Leader><Leader><tab> :set tabstop=4<CR>:set shiftwidth=4<CR>:set softtabstop=4<CR>:retab!<CR>

    nmap <Leader>: :!
    nmap <Leader>; :!

    map <Leader><leader>h <C-w>h
    map <Leader><leader>l <C-w>l
    noremap <Leader><Leader>j <C-w>j
    noremap <Leader><Leader>k <C-w>k
    noremap <S-h> gT
    noremap <S-l> gt

    noremap <leader>r :Q -args
    " noremap <leader>e :e .<CR>

    noremap <leader>v :vim<space>
    noremap <leader><leader>v :cexpr ''<CR>:cclose<CR>:bufdo vimgrepadd<space>
    noremap <leader><leader>c :cclose<CR>
    map ,d :vertical diffsplit #

    nnoremap x "_x
    nnoremap s "_s

    "置換ヤンクを連続でできるように"
    xnoremap <expr> p 'pgv"'.v:register.'y`>'

    noremap zo zO
    noremap zc zC
    noremap zr zR
    noremap zm zM

    noremap ; :

    "default completion
    inoremap ,. <C-x><C-n>


" -------------
" Plugin Setup
" -------------

    " ----------------
    " for easy motion

        map <Leader> <Plug>(easymotion-prefix)
        let g:EasyMotion_do_mapping = 0 " Disable default mappings

        map f <Plug>(easymotion-bd-fl)
        map t <Plug>(easymotion-bd-tl)
        map s <Plug>(easymotion-s2)
        map <leader>/ <Plug>(easymotion-w)


    " -----------------------
    " NERDTree keymap + icon

        " nnoremap <silent><C-e> :NERDTreeToggle<CR>
        " nnoremap <silent><Leader>e :NERDTreeToggle<CR>
        let g:NERDTreeDirArrows = 1
        let g:NERDTreeDirArrowExpandable  = '・'
        let g:NERDTreeDirArrowCollapsible = '▼'

        " NERDTress File highlighting
        function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
             exec 'autocmd filetype nerdtree highlight ' . a:extension .' ctermbg='. a:bg .' ctermfg='. a:fg .' guibg='. a:guibg .' guifg='. a:guifg
             exec 'autocmd filetype nerdtree syn match ' . a:extension .' #^\s\+.*'. a:extension .'$#'
        endfunction
        call NERDTreeHighlightFile('py',     'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('md',     'blue',    'none', '#3366FF', '#151515')
        call NERDTreeHighlightFile('yml',    'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('config', 'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('conf',   'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('json',   'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('html',   'yellow',  'none', 'yellow',  '#151515')
        call NERDTreeHighlightFile('styl',   'cyan',    'none', 'cyan',    '#151515')
        call NERDTreeHighlightFile('css',    'cyan',    'none', 'cyan',    '#151515')
        call NERDTreeHighlightFile('rb',     'Red',     'none', 'red',     '#151515')
        call NERDTreeHighlightFile('js',     'Red',     'none', '#ffa500', '#151515')
        call NERDTreeHighlightFile('php',    'Magenta', 'none', '#ff00ff', '#151515')

        " NERDTree 起動設定
        let g:NERDTreeShowBookmarks=1
        " autocmd vimenter * NERDTree
        " autocmd StdinReadPre * let s:std_in=1
        " autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif


    " ------------
    " Neocomplete

    "     " Disable AutoComplPop.
    "     let g:acp_enableAtStartup = 1
    "     " Use neocomplete.
    "     let g:neocomplete#enable_at_startup = 1
    "     " Use smartcase.
    "     let g:neocomplete#enable_smart_case = 1
    "     let g:neocomplete#disable_auto_complete = 1

    "     " Set minimum syntax keyword length.
    "     let g:neocomplete#sources#syntax#min_keyword_length = 3
    "     let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'
    "     let g:neocomplete#auto_completion_start_length = 3

    "     " Define dictionary.
    "     let g:neocomplete#sources#dictionary#dictionaries = {
    "         \ 'default' : '',
    "         \ 'vimshell' : $HOME.'/.vimshell_hist',
    "         \ 'scheme' : $HOME.'/.gosh_completions'
    "             \ }

    "     " Define keyword.
    "     if !exists('g:neocomplete#keyword_patterns')
    "         let g:neocomplete#keyword_patterns = {}
    "     endif
    "     let g:neocomplete#keyword_patterns['default'] = '\h\w*'

    "     " Plugin key-mappings.
    "     inoremap <expr><C-g>     neocomplete#undo_completion()
    "     inoremap <expr><C-l>     neocomplete#complete_common_string()

    "     " Recommended key-mappings.
    "     " <CR>: close popup and save indent.
    "     inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
    "     function! s:my_cr_function()
    "         return (pumvisible() ? "\<C-y>" : "" ) . "\<CR>"
    "         " For no inserting <CR> key.
    "         " return pumvisible() ? "\<C-y>" : "\<CR>"
    "     endfunction

    "     " <TAB>: select in complete.
    "     inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

    "     " <C-h>, <BS>: close popup and delete backword char.
    "     inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
    "     inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"

    "     "" 補完候補が表示されている場合は確定。そうでない場合は改行
    "     inoremap <expr><CR>  pumvisible() ? neocomplete#close_popup() : "<CR>"

    "     " 補完が止まった際に、スキップする長さを短くする
    "     let g:neocomplete#skip_auto_completion_time = '0.2'

    "     " 使用する補完の種類を減らす
    "     " 現在のSourceの取得は `:echo keys(neocomplete#variables#get_sources())`
    "     " デフォルト: ['file', 'tag', 'neosnippet', 'vim', 'dictionary', 'omni', 'member', 'syntax', 'include', 'buffer', 'file/include']
    "     let g:neocomplete#sources = {
    "       \ '_' : ['vim', 'omni', 'include', 'buffer', 'file/include']
    "       \ }

    "     " 特定のタイミングでのみ使う補完は、直接呼び出すようにする
    "     " 手動呼び出し
    "     inoremap <expr>,, neocomplete#start_manual_complete()
    "     " directory, fileだけを検索
    "     inoremap <expr>,/  neocomplete#start_manual_complete('file')
    "     " inoremap <expr><leader><leader>d  neocomplete#start_manual_complete('dictionary')
    "     " inoremap <expr><leader><leader>s  neocomplete#start_manual_complete('neosnippet')

    "     "Enable omni completion.
    "     autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
    "     autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
    "     " autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
    "     autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
    "     autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
    "     autocmd FileType fortran set omnifunc=fortrancomplete#Complete
    "     " autocmd FileType python set omnifunc=pythoncomplete#Complete
    "     autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
    "     autocmd FileType php set omnifunc=phpcomplete#CompletePHP
    "     autocmd FileType sql set omnifunc=sqlcomplete#Complete
    "     autocmd FileType c set omnifunc=ccomplete#Complete

        " jedi-vim と連携するときはこれonにするとエラー
        " Enable heavy omni completion.
        " if !exists('g:neocomplete#sources#omni#input_patterns')
        " let g:neocomplete#sources#omni#input_patterns = {}
        " endif

        "let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
        "let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
        "let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'


    " --------------------------------
    " for w/ jedi-vim and neocomplete

        " "1. 内部のomnifuncに渡す用オプション (omnifunc のデフォルトはc-x c-o?)
        " " autocmd FileType vim80-kaoriya-win64ython setlocal omnifunc=jedi#completions
        " " configuration をoff にしたらこれ必須
        " autocmd FileType python setlocal omnifunc=jedi#completions
        " " docstringは表示しない
        " autocmd FileType python setlocal completeopt-=preview

        " " 勝手にキーバインド設定やら自動補完などをやるので潰しておく
        " " initialize をoffにすると、下のコマンド設定が聞かなくなる
        " " OFFにすると一部補完文字化けして消えなくなるから、tabstop犠牲にしてもONにするしかない
        " " let g:jedi#auto_initialization = 0
        " " keymap系
        " " let g:jedi#smart_auto_mappings = 0
        " " " tabstop等 <SNR>keymapも
        " " let g:jedi#auto_vim_configuration = 0
        " " let g:jedi#completions_enabled = 0

        " " 2. これを入れると、neocompleteを起動したときにjedi-vimが立ち上がる
        " " autoの際のnc側の字数制限は効かない、
        " " auto_vim_configurationをoffにしたらomnifuncに渡すために1を入れる必要あり。
        " if !exists('g:neocomplete#force_omni_input_patterns')
        "         let g:neocomplete#force_omni_input_patterns = {}
        " endif
        " let g:neocomplete#force_omni_input_patterns.python = '\h\w*\|[^. \t]\.\w*'


    " -------------
    " for jedi-vim

        " let g:jedi#completions_command = '<C-Space>'
        " let g:jedi#rename_command = ',r'
        " let g:jedi#goto_command = ',g'
        " let g:jedi#usages_command =',j'
        " let g:jedi#goto_assignments_command = ',a'

        " vmap ,r :call jedi#rename_visual()<CR>
        " nmap ,r :call jedi#rename()<CR>
        " map ,j :call jedi#usages()<CR>
        " " the difference between assignment and definition: the latter is recursive serach
        " " map ,a :call jedi#goto_assignments()<CR>
        " map ,a :call jedi#goto_definitions()<CR>
        " map ,h :call jedi#show_documentation()<CR>


    " --------------
    " for syntastic

        " recommended settings
        " see https://github.com/scrooloose/syntastic#settings
        set statusline+=%#warningmsg#
        set statusline+=%{SyntasticStatuslineFlag()}
        set statusline+=%*

        let g:syntastic_always_populate_loc_list = 1
        let g:syntastic_enable_signs = 1
        let g:syntastic_auto_loc_list = 1
        let g:syntastic_check_on_open = 0
        let g:syntastic_check_on_wq = 0
        " flake8はpycodestyleに変更
        let g:syntastic_python_checkers = ['pycodestyle', 'pyflakes']
        let g:syntastic_python_pycodestyle_args="--max-line-length=120"

        " passive, activeで自動手動の設定
        let g:syntastic_mode_map = {
            \ "mode": "passive",
            \ "active_filetypes": [],
            \ "passive_filetypes": ["python"] }

        " 手動用のkey map
        noremap <leader>s :w<CR>:SyntasticCheck<CR>
        noremap <leader><leader>s :SyntasticCheck exit<CR>


    " ----------------------
    " for vim-indent-guides

        " vimを立ち上げたときに、自動的にvim-indent-guidesをオンにする
        let g:indent_guides_enable_on_vim_startup = 1


    " ----------------------
    " for w/ pyenv jedi-vim

        " if OSTYPE == "Linux\n"

        "     if jedi#init_python()
        "       function! s:jedi_auto_force_py_version() abort
        "         let g:jedi#force_py_version = pyenv#python#get_internal_major_version()
        "       endfunction
        "       augroup vim-pyenv-custom-augroup
        "         autocmd! *
        "         autocmd User vim-pyenv-activate-post   call s:jedi_auto_force_py_version()
        "         autocmd User vim-pyenv-deactivate-post call s:jedi_auto_force_py_version()
        "       augroup END
        "     endif

        " endif


    " -----------------
    " for Unite vim

        " The prefix key.
        let g:unite_enable_start_insert=1
        " nnoremap    [unite]   <Nop>
        nmap    <leader>u [unite]

        " unite.vim keymap
        let g:unite_source_history_yank_enable =1
        "スペースキーとcキーでカレントディレクトリを表示
        nnoremap <silent> <leader>c :<C-u>UniteWithBufferDir -buffer-name=files file file/new<CR>
        "スペースキーとfキーでバッファと最近開いたファイル一覧を表示
        nnoremap <silent> <leader>f :<C-u>Unite<Space>buffer file_mru<CR>
        "スペースキーとdキーで最近開いたディレクトリを表示
        nnoremap <silent> <leader>d :<C-u>Unite<Space>directory_mru<CR>
        "スペースキーとbキーでバッファを表示
        nnoremap <silent> <leader>b :<C-u>Unite<Space>buffer<CR>
        "スペースキーとrキーでレジストリを表示
        nnoremap <silent> <leader>r :<C-u>Unite<Space>register<CR>
        "スペースキーとtキーでタブを表示
        nnoremap <silent> <leader>t :<C-u>Unite<Space>tab<CR>
        "スペースキーとhキーでヒストリ/ヤンクを表示
        nnoremap <silent> <leader>y :<C-u>Unite<Space>history/yank<CR>
        "スペースキーとoキーでoutline
        nnoremap <silent> <leader>o :<C-u>Unite<Space>outline<CR>
        "スペースキーとENTERキーでfile_rec:!
        nnoremap <silent> <leader><CR> :<C-u>Unite<Space>file_rec:!<CR>
        "history"
        nnoremap <silent> [unite]h :<C-u>Unite<Space>history/unite<CR>
        "unite grep
        nnoremap <leader>g :<C-u>Unite grep:
        "unite grep/git"
        nnoremap <leader><leader>g :<C-u>Unite grep/git:
        "bookmark add"
        nnoremap [unite]a :UniteBookmarkAdd<CR>
        "bookmark"
        nnoremap [unite]b :Unite bookmark<CR>

        " unite-grepの便利キーマップ
        vnoremap /g y:Unite grep::-iRn:<C-R>=escape(@", '\\.*$^[]')<CR><CR>


    " -----------------
    " for unite-outline

        let g:unite_data_directory='~/.vim/bundle/unite.vim'
        let g:unite_abbr_highlight='Normal'
        " let g:unite_outline_filetype_options = {
                " heading': '^"'
        " }
        " let g:unite_source_outline_info.ruby = {
        "       \ 'heading': '^\s*\(module\|class\|def\)\>',
        "       \ 'skip': {
        "       \   'header': '^#',
        "       \   'block' : ['^=begin', '^=end'],
        "       \ },
        "       \}


    " -------------
    " for VimFiler

        let g:vimfiler_as_default_explorer=1
        let g:vimfiler_enable_auto_cd = 1
        noremap <leader>e :VimFiler<CR>


    " --------------
    " for braceless
        autocmd FileType python BracelessEnable +indent


    "
    " --------------
    " quickrun

        let g:quickrun_config = {
        \   '*': {'runmode': 'async:remote:vimproc'}
        \ }

" -----------------
" for win and linux
" -----------------

    if OSTYPE == "Darwin\n"
       ""ここにMac向けの設定
    elseif OSTYPE == "Linux\n"
       ""ここにLinux向けの設定
    else
        "-----------setting for gvim-------------"
        set guioptions-=m
        set guioptions-=T
        set guioptions-=r
        set guioptions-=R
        set guioptions-=l
        set guioptions-=L
        set guioptions-=b

        "バックアップファイル(~)を作成しない
        set nobackup
        set noundofile

        "!でのshell起動
        set shell=C:\\\"Program\ Files\"\Git\usr\bin\bash.exe

        " set imdisable
        set iminsert=0
        set imsearch=-1
        "quickrun用
        let g:quickrun_config = {
        \ "_": {
        \ "runner": "vimproc",
        \ "runner/vimproc/updatetime": 40,
        \ },
        \   'python': {'command': 'C:\\Anaconda3\\python.exe'}
        \ }
        :finish


        "for python
        " set pythondll="C:\Python27\python27.dll"

    endif

