#!/bin/bash
nproc=16  # Number of processor
workdir=`pwd`

abinit_dir=~/ABINIT
tcl_script=mkinp_ver7_0.tcl

fname=${1%.*}
ajfname=$fname.ajf
inpname=$fname.inp
outname=$fname.out

cd $workdir

$abinit_dir/tools/$tcl_script < $ajfname > $inpname
bsub -n $nproc -i $inpname -o $outname mpirun \
     -n $nproc $abinit_dir/bin/abinitmp

