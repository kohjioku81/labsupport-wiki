# vim コマンド  
## 基本コマンド  
基本コマンドとしては以下を最初に覚えることになります.  
* `i`: インサートモードに入る  
vimには3つのモードがあり、入力するためにはインサートモードに入る必要があります。  
`i`を押すことでインサートモードに入ります  
* `Esc`: ノーマルモードに戻る  
インサートモードで編集を終えたら、`Esc`コマンドでノーマルモードに戻ります。  
* `wq`: 保存して閉じる  
ノーマルモードで、:wqと押すと「保存して閉じる」の操作になります。  
wが保存、qが閉じるで、それぞれ独立してつかうことができ、  
`:q!`と打つと、保存しないで閉じるができます。  
最低この3コマンドで作業ができます。  
  
その他無数の操作があり、以下一例です。  
### 移動   
カーソルの移動は矢印キーでもできますが、本来は`h`(左)`j`(下)`k`(上)`l`(右)で行います。  
`w`, `b` で単語単位の移動、バックも可能です。  
  
### ページ送り  
`Ctrl + d(f)` で半(全)ページ下に、`Ctrl + u(b)` で半(全)ページ上に移動できます。  
  
### 選択、コピー  
`v`でvisualモードに入り、カーソル移動で選択ができます。  
選択範囲をきめて`y`を押すことでコピー(yank)ができます。  
貼り付けは`p`ボタンです。  
  
### 任意の位置からのインサートモード入り  
`A`: 行末からインサートモード  
`a`: 次の文字からインサートモード  
`ce`: 単語一つ分けしてインサートモード  
  
### 一括コメントアウト  
プログラムの一括コメントアウトに便利  
`ctrl-v 矩形選択　下まで進んで　shift i # esc`　一括でコメントアウト  
  
### 戻す, 次にすすむ  
`u`, `Ctrl + r` で履歴の前後ができます  
  
### 文頭、文末  
`gg`: 文頭へ移動  
`shift + g`: 文末へ移動  
  
### 画面の縦分割, 移動  
`vs`: 縦に分割  
`ctrl + ww`: 画面移動  
  
その他操作はレベル別に解説してくれている[ここ](https://qiita.com/hachi8833/items/7beeee825c11f7437f54)がおススメです。  
  
## プラグインやオリジナル設定も含めたキーバインド  
[ここ](vim_edit.md)の通りに、サンプルの.vimrcの設定を行うと、以下のような操作が可能です。  
  
## ファイル操作(unite, neomru, neoyank, vimproc, vimfiler)  
`space + f` 現在開いてるファイルと、開いた履歴  
`q` 抜ける  
`space + c` 現在のディレクトリのファイル表示  
`space + y` yankの履歴  
`space + u + b` ブックマーク表示  
ブックマーク登録方法  
`space + c` で開いて登録したいディレクトリを指定 -> `tab` -> `bookmark` -> `path(そのまま)` -> `名前(任意)`  
  
# カーソル移動(easymotion)  
任意の位置にカーソル移動ができます  
`s` 後に `検索したい二文字`  
  
## 構文チェック(syntastic)  
書いたプログラムの構文をチェックできます  
`space + s` 起動, 更新  
`space + space + s` 終了  
  
## プログラム簡易実行(quickrun)  
`:Q` でvimを開いたまま簡単にプログラムを実行できるようになります。  
  
## 折り畳みの開閉  
`zr` すべて開く  
`zm` すべて閉じる  
`zo` そこを開く  
`zc` そこを閉じる  
  
## 短縮操作  
`jj` insertモードから抜ける  
`space space h or l` 分割した画面間の移動  

