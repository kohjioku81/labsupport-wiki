# FMO計算
FMO計算は分子をフラグメントに分割して行う量子化学計算です。  
望月研では望月先生が取りまとめているABINIT-MPというプログラムを使います。  
  
## 必要ファイル  
FMO計算を行うためには以下のファイルを準備する必要があります。  
* 計算対象構造ファイル(pdb形式)  
* 計算設定ファイル(ajfファイル)  
    - 計算方法/基底関数などの条件  
    - フラグメントの切断情報  

## 手順
1. 計算対象構造ファイルの準備（pdb）  
タンパク質の場合、Protein data bankなどからデータを持ってきます。  
2. 入力条件ファイル（インプットファイル）を準備  
BioStation Viewer(現行の最新版は16.02)を使用して作成します。ajf(abinit job file)と呼ばれます。  
BSVと略され、会話でも"ビーエスブイ"といわれることも多いです。  
  
3. abinitmpによる計算  
abinitmpにはWindowsもありますが、計算資源を多く使うため、基本的にはLinuxで使用します。  
`abinitmp -n コア数 < ajfファイル名 > 任意のoutファイル名`で実行します  
基本的にabinitmpが導入されているマシンは[Lavaのジョブ管理システム](command.md)が入っているため  
`bsub -n コア数 "abinitmp -n コア数 < ajfファイル名 > out名"`で実行します。  

詳しい仕様は[BSVのマニュアル](../data/BSV_v16man.pdf), [ABINIT-MPのマニュアル](../data/ABINIT-rev5man.pdf), または[ここ](../data/abinit_簡易資料_20150416.pdf)や[この辺り](../data/ABINIT-MP_Open1_Rev5_仕様.pdf)のドキュメントを参照してください。

### バージョンによる整形の必要
ABINIT-MPはバージョンが複数あり、バージョンごとに入力ファイルの形式がちょくちょく変わります。
現行版はOpen版Ver.1 Rev.10となっていますが、附属のTclやpythonスクリプトで、バージョンに合わせた整形を行う必要があります。

### 実行スクリプト
上記のような煩雑さがあるため、実使用はシェルスクリプトを用い、ajfを整形して流すという処理を一括で行うことが多いです。  
[このようなスクリプト](runab.sh)を組み、プログラム内部で整形スクリプトのパスを指定し実行します。  
(このスクリプトは~/ABINIT/bin/内にabinitmpバイナリ(Ver.1, Rev.5), ~/ABINIT/tools内に[整形スクリプト(mkinp_ver7_0.tcl)](../data/mkinp_ver7_0.tcl)を入れた状態での実行を想定しています。)  
`runab.sh ajf名` といったコマンドで実行できます。  

### サンプルデータ
グリシン5量体を計算例とした際  
[gly5.ajf](../data/gly5/gly5.ajf)と[gly5.pdb](../data/gly5/gly5.pdb)を使用してabinitmpを実行すると[gly5.cpf](../data/gly5/gly5.cpf), [gly5.out](../data/gly5/gly5.out)といった結果が得られます。  

## 解析  
cpfをifieテーブルを読み込む  
こんな感じの可視化が可能です。  
フラグメント間の相互作用(IFIE(アイフィー)といいます)の強さが色で可視化され、黄色のフラグメントに対し安定化の強い配置が赤色、反発の強い配置が青色で示されます。  
具体的な数値もリストで出力可能です。  
![ifie](../data/gif/ifie_cpf.gif)  

## 文献
文章化するときはこの辺りを引く必要があります。
### FMO
* [K. Kitaura et al., Chem. Phys. Lett. 313, 701 (1999).](../data/paper/kitaura1999.bib)
* [Fedorov and K. Kitaura, The FRAGMENT MOLECULAR ORBIAL METHOD: Practical Applications to Large Molecular Systems (CRC Press, Boca Raton, 2009).](../data/paper/kitaura2009.bib)

### ABINIT-MP
* [ABINIT-MP: S. Tanaka et al., Phys. Chem. Chem. Phys. 16, 10310 (2014).](../data/paper/tanaka2014.pdf)

## 特殊な設定
### 溶媒効果の導入
ajfの&FRAGPAIRネームリストの後に以下を追加することで、Poisson-Boltzmannの溶媒効果が導入されます。  
特に電荷を持つ残基の相互作用が緩和されますが、繰り返し計算を行うため、計算コストが3-10倍近くかかります。  
  
`&SOLVATION`  
`EFFECT='ON'`  
`ITRMOD='Normal'`  
`MAXITR=100`  
`THICNV=1.0E-2`  
`IGSCC='Core'`  
`INIEHF='OFF'`  
`PRBRAD=1.4`  
`EPSOUT=80.0`  
`EPSIN=1.0`  
`SRFTNS=0.0072`  
`SRFOFF=0.0`  
`NSPHER=1000`  
`SCREEN='ES+NP'.`  
`/`  
`&PBEQ`  
`MAXITR=1000`  
`JDGCNV='RMS'`  
`THRCNV=1.0E-5`  
`/`  
  
変える可能性ある場所の簡単な説明ですが  
* &SOLVATION  
THICNV: 収束の閾値で、デフォルトだと1.0E-5位、計算感がかかるため上記では緩めの閾値  
  
* &PBEQ  
原子種に一般のタンパク以外のものがあるとエラーがでるので  
MOLRAD='vdw'  
を追加  
といった形です。  
