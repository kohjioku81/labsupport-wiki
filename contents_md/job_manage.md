## ジョブ管理システム  
一つの計算サーバーに複数人がアクセスする場合、または複数台のマシンが連結している並列計算機(NISといいます)で計算する場合、以下のような理由でジョブ実行の管理を行う必要があります。  
* 他人のジョブが流れている際に追加でジョブを投入した際に、計算資源を超えて負荷がかかってしまう  
* (並列計算機で)管理マシンから計算マシンへ計算指示を出す  
  
その際、あらかじめ導入したジョブ管理システムに計算ジョブの指示を出すことで、以下のような処理を自動で行ってくれます。  
* 他人のジョブが終了してから流す  
* 計算の優先度をつけ、優先度の高いジョブから流す  
* 空いた計算コアに自動でジョブを投入する  
  
ジョブ管理システムには概ね「LSF系」「PBS系」の二種類があり、望月研では機能が制限された無料版のLava(LSF系), Torque(PBS系)がそれぞれ一部のマシンに導入されています。  
現在導入されているのは以下のサーバです。  
  
## Lava  
望月研では以下のサーバに導入されています。  
* gaussianサーバ(ukon, sage, onion)  
* FMO計算用サーバ(vanilla, chem2, hpcs01, iris)  
  
### 実行  
* `bsub`: `bsub オプション コマンド` で実行できます。  
オプションとしてよく使用するものとしては  
    * `-n`: 使用コア数  
    * `-m`: 使用ノードの指定  
などがあります。  
  
### 管理  
* `bjobs`: 投入されているジョブの確認  
よく使うオプションとして  
    * `-u all`: ユーザ名の指定（allですべてのユーザのジョブ確認ができます)  
    * `-a`: 終了したジョブ情報を一定期間表示  
    * `-w`: 省略しないフォーマットで表示  
    * `-l`: 詳細情報表示  
* `bhosts`: 各計算ノードで使用中のコア情報を出力  
* `bkill ジョブID`: ジョブの削除  
* `bpeek ジョブID`: 実行中のジョブの途中経過を表示します。  
引数がなければ最後に投入したジョブを表示します。  
そのまま実行すると一気に表示されてしまうため、`bkeek ジョブID | less`とするのが便利です  
* `bqueues`: 各キューに投入されたジョブ数の確認  
* `bmod -q キュー名 ジョブID`: 投入したジョブのキュー変更  
![lava](../data/gif/lava.gif)  
その他詳細は[こちら](https://www.hpc.co.jp/lsf_bhosts.html)や[こちら](../data/LavaManual.pdf)を参照

## Torque  
望月研では以下のサーバに導入されています。  
* 多種計算用サーバ(gingera0)  
* 機械学習用サーバ(hydrogen, helium)  
  
### 実行  
* `qsub`: `qsub オプション シェルスクリプト` で実行できます。  
よく使うオプションですが  
    * `-l`: 使用ノード、コア数を指定します。`-l nodes=1:ppn=8`のような形で実行します。  
    * `-o 任意のファイル名`: 結果のファイル名を指定します。  
    * `-e 任意のファイル名`: エラー出力ファイル名を指定します。  
シェルスクリプト内にオプションをまとめて記述することも可能です。  
`#PBS -l nodes=1:ppn=8`  
`#PBS -o stdout.log`  
`#PBS -e stderr.log`  
のように記述します。  

### 管理
* `qstat`: 投入されているジョブを確認できます。  
* `pbsnodes`: 並列計算機として連結されているマシンの情報を確認することができます。  
* `qdel ジョブid`: 指定したジョブを終了させます  
その他詳細は[こちら](../data/Torque-man.pdf)  
  
### サンプル
pythonを実行する際のサンプルです。  
[実行ファイル](../data/Torque_sub.sh)と[pythonファイル](../data/test.py)を同一ディレクトリに置き、`qsub Torque_sub.sh`と実行してみてください。  

## ジョブ管理システムを使用しないジョブ投入でのジョブ管理
上記ジョブ管理システムを実行しない場合, 投入制限等の管理はできませんがバックグラウンド実行を行うことで、ジョブを投入したままコンソールの操作ができ、投入したジョブの管理ができます。  
  
### バックグラウンド実行  
コマンドの最後に`&`をつけて実行  
  
### 管理  
* `jobs`: 自分が操作しているシェル上で起動しているジョブの確認  
自分が今操作しているコンソールをカレントシェルといいますが、カレントシェルで実行されているジョブが表示されます  
* `ps`: 今自分が操作しているシェルに関連したプロセスで起動している処理の確認  
言葉がわかりずらいですが、jobsより広い範囲での実行処理がでます  
* `top`: サーバー上で動いているプロセス等の確認  
自分に限らず、サーバー上で動いているプロセスや、その他多くの情報が出ます。  
* `kill`: `kill プロセスid`で実行中のジョブを終了させることが出来ます。  

