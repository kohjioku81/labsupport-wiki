## Openbabelとは  
分子構造のコンバートを中心に、力場による最適化など様々な処理を行えるフリーソフトです。  
  
### 形式コンバート  
分子構造は様々なファイル形式によって書かれます。(xyz, pdb, mol, gjfなどなど)  
* あるソフトではこの形式しか使えない  
* 自分が作ったプログラムで扱える形式にしたい  
といったときにopenbabelを使うと一発でコンバートすることができます。  
例えばこの研究室だと、  
* ABINIT-MPのinput: pdbファイル  
* FCEWSのinput: xyz  
* gaussian: gjf  
といったように特定の構造ファイルの指定があるため、コンバートが楽にできることは非常に大事になります。  
gaussianのoutファイルから構造最適化後の構造を抜き出して任意の形式にすることも可能です。  
  
## SMILES対応  
SMILES表記とは分子構造を文字列として表したもの（示性式のような形で構造式を書いたもの)で、情報化学を扱うときに頻出する形式です。openbabelでは式の情報から3次元構造を再現することができます。  
  
## インストール(Ver. 2.4.1)  
windows: [ここ](https://sourceforge.net/projects/openbabel/files/openbabel/2.4.1/OpenBabel-2.4.1.exe/download)からインストール可能です。GUI操作もできます。  
Linux: 各サーバーに古いバージョンは入っていますが、一部機能が使えないため、新バージョンのインストールをオススメします。  
* [ここ](https://sourceforge.net/projects/openbabel/files/openbabel/2.4.1/openbabel-2.4.1.tar.gz/download)からtar.gzをダウンロード  
* [ここ](https://openbabel.org/docs/dev/Installation/install.html#compiling-open-babel)のBasic build procedure, Local build項を参考にしてください。  
[Wikiサイト](http://openbabel.org/wiki/Category:Installation)にもインストールに関する情報があります。  
  
## コマンド  
### 変換  
一般的にはこのコマンドのみ把握すれば大丈夫です。  
`babel -iインプット形式 インプットファイル名 -oアウトプット形式 アウトファイル名`  
pdb からxyzへの変換であれば以下のようになります。  
`babel -ipdb xxx.pdb -oxyz xxx.xyz`  
  
### 変換(SMILES->3D)  
SMILESから三次元座標への変換には--gen3dオプションを使います。  
`babel -ismi xxx.txt -opdb xxx.pdb --gen3d`  
例えばベンゼンのsmiles表記はc1ccccc1となりますが、[これを記したファイル](../data/benzene.smi)をopenbabelにかけると[このような](../data/benzene.pdb)ファイルが得られます。  
`babel -ismi benzene.smi -opdb benzene.pdb --gen3d`  
  
  
### 構造最適化  
`obminimize` というコマンドを使います。  
`obminimize オプション インプット名 > アウトファイル名`
オプションとしては以下のものがあります
`-n 数字`: 最適化ステップ数
`-ff 力場名`: 力場の種類の指定(GAFF, UFF等)

その他[ここ](https://openbabel.org/docs/dev/Command-line_tools/babel.html)や[この辺](https://openbabel.org/wiki/Obminimize)を参考にしてください。
