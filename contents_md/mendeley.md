# Mendeley

研究において資料を作る際に文献の引用が必要となりますが、面倒なフォーマットが決まっています。文献管理と共にその処理を自動でやってくれるのがmendeleyです。  
wordのプラグインもあるため、非常に便利です。  
  
## 導入、使い方  
[ここ](http://live-reasonably.blogspot.com/2015/05/mendeley.html)や[これ](../data/mendeley.pdf)に細かく載っていますので見てみてください。ざっくりいうと以下の流れです。  
  
* [登録、ダウンロード](https://www.mendeley.com/?interaction_required=true)  
* ドロップすることでpdfをいれる  
* DOIを読み込んで正式なデータをロード  
* View->Citation Styleで論文のスタイルを指定  
* 右クリック-> Copy as -> Formatted Citationでコピー  
* 好きな文章に貼り付け  
別PCでも共有できる他、著者ごとに分けてデータを保管、リネームしてくれるため整理にも便利です。  
  
##  wordとの連携  
詳しくは[こちら](word.md)をご覧ください  

