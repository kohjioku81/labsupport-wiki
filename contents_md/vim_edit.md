# vim プラグイン  
  
neobundleというプラグインをいれることで、様々な便利機能を導入することができます。  
詳しくは[こちら](https://qiita.com/Kuchitama/items/68b6b5d5ed40f6f96310)を参照してもらえればと思いますが、基本的に以下の操作です。  
  
1.  ホームディレクトリ以下.vimフォルダ以下にbundleフォルダを作成  
`mkdir -p ~/.vim/bundle`  
2.  作成したbundleフォルダにneobundle.vimをクローン  
`git clone git://github.com/Shougo/neobundle.vim ~/.vim/bundle/neobundle.vim`  
  
一から設定する人は上記URLに従ってもらえればできますが、私の設定ファイルを共有しておきます。  
3. [ここの.vimrc](../data/.vimrc)をホームディレクトリに配置  
`cp .vimrc ~/`  
4. vimで.vimrcを開き、プラグインをインストール  
`vim .vimrc`  
`:NeoBundleInstall`  
インストール後、以下のような機能が使えるようになります。  
  
## ファイル操作(unite, neomru, neoyank, vimproc, vimfiler)  
`space + f` 現在開いてるファイルと、開いた履歴  
`q` 抜ける  
`space + c` 現在のディレクトリのファイル表示  
`space + y` yankの履歴  
`space + u + b` ブックマーク表示  
ブックマーク登録方法  
`space + c` で開いて登録したいディレクトリを指定 -> `tab` -> `bookmark` -> `path(そのまま)` -> `名前(任意)`  
  
# カーソル移動(easymotion)  
任意の位置にカーソル移動ができます  
`s` 後に 検索したい二文字　  
  
## 構文チェック(syntastic)  
書いたプログラムの構文をチェックできます  
`space + s` 起動, 更新  
`space + space + s` 終了  
  
## プログラム簡易実行(quickrun)  
`:Q` でvimを開いたまま簡単にプログラムを実行できるようになります。  
  
## インデントガイド(indent-guides)  
4文字のインデントごとに色がついて見やすくなります.  
  
## 配色(molokai)  
molokaiという見やすい配色設定になります。  
  
## ステータスバー(lightline)  
整ったステータスバーが表示できます  
  
## python 便利操作  
別の設定(lua, python連携)が必要なため共有したファイルではoffにしていますが、適切に設定にすると以下のようなジャンプ、補完機能が可能です。  
### ジャンプ(jedi-vim)  
(normal modeで)  
`, + j` 関数の位置にジャンプ  
`, + a` 関数の定義に移動(assignment)  
  
### 補完(neocomplete, jedi-vim)  
(insert mode)  
`, + ,` python モジュール名の補完  
`, + .` 変数名等の補完（キーワード）  
`, + /` ディレクトリ名の補完  
  
# ユーザ設定  
プラグイン以外にも  
.vimrcに設定を書き込むことで、細かな設定を行うことができます。  
  
## 便利機能の設定  
`set` で様々な条件を指定します  
以下一例です。  
* 行数の表示  
`set number`  
* 自動インデントの桁数設定  
`set shiftwidth=4`  
* タブ押時に自動で半角スペースに変換した上で桁数を指定  
`set softtabstop=4`  
* インデントごとに自動でプログラムを折り畳む  
`set foldmethod=indent`  
※この際、以下の処理で開閉ができます。  
`zr` すべて開く  
`zm` すべて閉じる  
`zo` そこを開く  
`zc` そこを閉じる  
  
## キーマップの作製  
ユーザが好きなキーに各機能を割り当てることができます。  
* `jj` にエスケープを割り当てる  
`inoremap <silent>jj <Esc>`  
これにより、インサートモードから`jj`で抜けられるようになります。  
* `space + h` で行の文頭, `space + l`で行末に  
`noremap <space>h ^`  
`noremap <space>l $`  


