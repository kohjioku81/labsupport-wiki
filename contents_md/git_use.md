## git 使い方  
  
1. リポジトリの作成、管理  
    1. git管理をしたいディレクトリに入る  
    `cd プログラムのディレクトリ`  
    1. リポジトリ作成  
    `git init`  
    1. リポジトリ内の登録ファイル情報の確認  
    `git status`  
    1. 登録するファイルを決定  
    `git add ファイル名`  
    (再帰的にやる場合はgit add .)  
  
    ※この際、.gitignore にaddしたくないファイルを書く  
    e.g. .gitignore  
    00*  
    01*  
    test  
    bin  
  
    1. リポジトリに登録(コミット)  
    `git commit -m first_commit`  
  
    1. commitのログを確認  
    `git log`  
  
    1. 直前のコミットと現在の差をみる  
    `git diff HEAD^`  
  
    色々なコマンドがあるので[この辺](https://qiita.com/shibukk/items/8c9362a5bd399b9c56be)を参考にしてください  
  
  
## 共有用の設定(dropbox)  
共有のためには「リモートリポジトリ」と呼ばれる、実際にファイルを登録するわけではない共有用リポジトリをクラウド上に配置します。  
1. dropboxのインストール  
linuxであれば、[ここ](https://www.dropbox.com/ja/install-linux)の  
「コマンド ラインを使った Dropbox のヘッドレス インストール」に従う  
  
1. 共有リポジトリの作成  
    1. 共有用のディレクトリをdropbox内のどっかすきな場所につくる  
    2. その中に@@.gitってディレクトリつくって、`git init --bare --share`する  
  
1. 共有したいデータ群（ローカルリポジトリ）のリモートへの登録  
    1. 共有したいデータが入ってるリポジトリ(ローカルリポジトリ)に入る  
    1. リモートリモートリポジトリを登録する  
    `git remote add origin 共有用リポジトリのあるディレクトリのパス //リモートリポジトリの設定`  
    (リモートリポジトリが別サーバにある場合: `git remote add origin ssh://USER_NAME@SERVER_NAME:PORT/共有用リポジトリまでのパス/repository/project.git`)  
    1. ローカルリポジトリから共有リポジトリにpushする  
    `git push origin master`  
  
1. リモートリポジトリからのデータの受取  
    1. 受け取りたいディレクトリでクローンする  
    `git clone リモートリポジトリへのパス`  
    1. リモート環境の更新  
    `git pull (origin master)`  
  
## 共有用の設定(gitlab)  
1. gitlabへの登録  
[ここ](https://about.gitlab.com/)からアカウントを作成します  
  
2. プロジェクトの作成  
[この辺](https://qiita.com/CUTBOSS/items/ce61bb6a8635c6918558)を参考にしてください。  


