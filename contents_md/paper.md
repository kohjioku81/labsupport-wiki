# 論文について  
研究者の仕事のゴールの一つが自身の研究を論文として出版することです。  
優れた研究をしても、発表して、文章として出さなければ対外的には「何もしていない人」になってしまいます。  
論文の形態にもいろいろありますが、一般的には学会が論文誌が取りまとめている雑誌に投稿し、「査読」という形で審査を受けて、受諾されれば掲載、という形になります。  
  
名前は聞いたことあるかと思いますが、ネイチャーみたいなのもそのうちの一つです。  
化学系としてはたとえば以下のような雑誌があります。  
* JACS  
* PENAS  
* JPC  
* JCP  
* PCCP  
* CPL  
* Macromolecules  
論文にはそれぞれIF(インパクトファクター)というものがあり、審査の厳しさが違ってきます。JACSなんかは計算がいくら頑張ってものることはないです。  
ドクターの卒業要件は、ある程度まともな査読付きの論文誌IFが3程度ある雑誌に2本程度掲載されることが一般的で、立教では1本でも問題なかったりします。  
とにかく学会発表とかと比べて、論文にするのはやたら面倒でコスパの悪い仕事です。が、必須です。  
  
## 論文投稿の流れ  
1. 投稿する雑誌をきめる  
本当はこれは研究を始める段階である程度目星をつけておくのが好ましいです。それぞれの雑誌に特色があります。  
  
2. 投稿雑誌に合わせて原稿作成  
論文の体裁はある程度は統一されてますが、詳細は投稿雑誌によってことなります。  
* 基本的な体裁  
    - abstract: 概要  
    - introduction: 背景  
    - simulation details: 実験方法(モデル設定、方法論)  
    - result and discussion: 結果と考察  
    - conclusion: 結論  
    - referrences: 参考文献  
    - (別紙)supporting information: 補足  
  
    一番面倒なのが背景です。  
    引用しながら業界の動向を述べていって、自分の研究にどんな意義があるのかを語っていきます。研究者に一番重要なのはこのストーリーテラーの能力、という人もいるくらいなので、作業が苦なくできるのであれば、研究者の素質多いにありだと思います。  
    参考文献テーブルでは文中で示した参考文献リストを一覧にして載せていきます。厄介なのがこのリストの記載の体裁は各雑誌によってことなるため、その雑誌に合わせた体裁で原稿を整える必要があります。  
    昔の人は手動で文献を管理していましたが、今はそれなりに便利なソフトがあり、無料でつかえる代表格がMendeleyというソフトです。  
    これで、本文の参照番号とrefferenceテーブルを関連付けて、体裁も整えて出力してくれます。(データも完璧ではないため、手動でいじる必要はあります）  
    ある文献を追加した際に本文中の全部の番号を自動で変更してくれるため、非常に便利です。(昔の人はこれを手作業でやってたとか信じられないです)  
    原稿のフォーマットは各雑誌のHPにテンプレートがありますので、落としてきたもののスタイルにはめていくのが一番確実かと思います。  
  
* 校正  
研究者はアイデア勝負、みたいに思われがちですが、校閲部があるわけでもないので、ちまちました校正も自分でやる必要があります。  
カンマのあと、括弧前後のスペースとかとか  
この辺が統一されてないと「一貫性がない、手を抜くな」とまず教員からお叱りがきます。  
  
3. 投稿  
各雑誌のHPから投稿欄があります。  
cover letter という、本文と別に概要を書いて、宜しくお願いします、みたいな形で投稿する必要があります。  
更に、希望のレビュアー（審査してくれる人）やアンチリスト(避けてほしい人)をこちらから指定する必要があります。この辺は先生と相談するのがよいです。  
  
4. 修正(リバイズ)  
1か月ほどすると査読者（通常2-3人)からコメントが帰ってきて、ここはどうなってるんだ、ここが意味不明だの文句を垂れてくるので、適宜説明、修正をして再投稿します。  
この際のレビュアーへの返答ドキュメントを別途用意の上、レビュアー1, 2の対応が区別できるように本文では原稿に色つけたりして対応します。  
リバイズの段階にいけば（程度によりますが)きちんと対応していけば最終的には掲載されます。掲載するに値しないと判断された場合は棄却(リジェクト)され、他の学会誌に出すことになります。。  
  
5. 最終チェック  
何回かの修正処理の後、acceptされると、整った形で原稿が送られてきます。(proofといいます)これをチェックして、返送して完了すると、暫くの後ネット上でアクセス可能になります。  


