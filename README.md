# 計算系 研究補助wiki

## はじめに
[研究室とは](contents_md/about_lab.md)  
[研究活動を行う上で](contents_md/check.md)  
[年間スケジュール](contents_md/schedule.md)  

## 研究実行
[計算の始め方](contents_md/setup.md)  
[linuxコマンド](contents_md/command.md)  
[linuxの予備知識](contents_md/linux_system.md)  
[ジョブ管理](contents_md/job_manage.md)  
[分子シミュレーションソフトウェア概要](contents_md/program.md)  
[論文を書くには](contents_md/paper.md)  
[学会発表](contents_md/gakkai.md)  
[スライドの作り方](contents_md/slide.md)  
[word文書の作り方](contents_md/word.md)  

## テーマ別プログラム、ソフトウェア実行
### 分子シミュレーション
[量子化学計算(Gaussian)](contents_md/gaussian.md)  
[FMO計算(ABINIT-MP, Biostation-Viewer)](contents_md/abinitmp.md)  
[分子動力学(OCTA(COGNAC))](contents_md/octa.md)  
[材料設計用パラメータ算定(FCEWS)](contents_md/fcews.md)  
[DPD設定支援ツール(DPDgen)](contents_md/dpdgen.md)  
[リバースマップ-FMO(DSRMS)](contents_md/dsrms.md)  
[生体分子の処理(MOE)](contents_md/moe.md)  
[分子データ処理ツール(Openbabel)](contents_md/babel.md)  

### ML, DL

### ドキュメント管理
[Mendeley](contents_md/mendeley.md)  

### その他

## プログラム作成環境
[vim](contents_md/vim.md)  
[vimコマンド](contents_md/vim_command.md)  
[vimの拡張](contents_md/vim_edit.md)  
[python環境構築](contents_md/python.md)  
[git](contents_md/git.md)  
[git使い方](contents_md/git_use.md)  

## その他
[printerの設定](contents_md/printer.md)  
[リモートでの作業環境構築](contents_md/remote.md)  
[奨学金(院生向け)](contents_md/scholar.md)  
